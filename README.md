# Update Repository for PV1000 HMI

This repository contains all files to update the PV1000 HMI on a phytec board.

## Old Updater compatibility

The files `.version` and `PVHMI` in the root are for the legacy updater.
They are crafted to make the legacy updater update the HMI and the updater to the most recent version which supports self updates.

